package main

import (
	"fmt"
	"log"
	"math"
)

/* print bits of 32-bit number */
func printAsBits(number uint32) {

	/* declaration of variables */
	var (
		div uint32
		err error
		i   int
	)

	/* write bits */
	div = 2147483648
	for div >= 1 {
		i++
		_, err = fmt.Print((number & div) / div)
		if i == 1 {
			fmt.Print(" ")
		} else if i == 9 {
			fmt.Print(" ")
		}
		handleError(err)
		div /= 2
	}

}

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to read float number with printing of message */
func readFloat(msg string, number *float32) {

	/* declaration of variables */
	var (
		err error
	)

	/* write message and read number */
	fmt.Println(msg)
	_, err = fmt.Scanln(number)
	handleError(err)

}

/* entry point */
func main() {

	/* declaration of variables */
	var (
		a, b, res    float32
		aI, bI, resI uint32
		exp          uint32
		mant         uint64
		sign         uint32
		iter         uint32
	)

	/* read numbers */
	readFloat("number a:", &a)
	readFloat("number b:", &b)

	/* convert numbers to uint32 */
	aI = math.Float32bits(a)
	bI = math.Float32bits(b)

	/* calculate order and mantissa */
	exp = (aI & uint32(2139095040)) >> uint32(23) + (bI & uint32(2139095040)) >> uint32(23)
	mant = uint64(aI & uint32(8388607) + uint32(8388608)) * uint64(bI & uint32(8388607) + uint32(8388608))
	exp = exp - 127

	/* normalize mantissa */
	iter = 22
	for ((mant & uint64(9223372036854775808)) / uint64(9223372036854775808) == 0) && (iter != 0) {
		mant <<= 1
		iter--
	}
	mant <<= 1
	mant >>= 41

	/* calculate sign */
	sign = ((aI & uint32(2147483648)) / uint32(2147483648) + (bI & uint32(2147483648)) / uint32(2147483648)) & uint32(1)

	/* calculate result */
	resI = sign * uint32(2147483648) + exp << uint32(23) + uint32(mant)
	res = math.Float32frombits(resI)

	/* write initial numbers in bit view */
	fmt.Println("Number a in bit view:")
	printAsBits(aI)
	fmt.Println()
	fmt.Println("Number b in bit view:")
	printAsBits(bI)
	fmt.Println()

	/* write result in bit and normal views */
	fmt.Println("Result in bit view:")
	printAsBits(resI)
	fmt.Println()
	fmt.Println("Result in normal view:")
	fmt.Println(res)

}
